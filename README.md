# MakiasakeWebapp

This is a project generated with Angular 7 to display showcase a little bit about me and what I do.
For more information about Angular see: [Angular CLI](https://github.com/angular/angular-cli).

![main-page-thumbnail](https://drive.google.com/uc?id=1yflLSOvOzT8HQXPZ6TjxPpEECV4uZMG_)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

#Code Versioning
Bitbucket repository: git@bitbucket.org:makiasake/makiasake-webapp.git

#Dependencies
Bootstrap, Jquery

#Check
https://getbootstrap.com/docs/4.3/getting-started/introduction/